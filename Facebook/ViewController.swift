//
//  ViewController.swift
//  Facebook
//
//  Created by JONNY on 9/5/1399 AP.
//

import UIKit

class ViewController: UIViewController {
    
    //create Object
    var obj = [Post]()
    var objStory = [Story]()
    var objNoImg = [NoImage]()
    
    @IBOutlet weak var tableView: UITableView!
    

    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // story object
        objStory.append(Story(story: "7", profile: "12", userNaem: "Hunmit"))
        objStory.append(Story(story: "8", profile: "13", userNaem: "Jonny"))
        objStory.append(Story(story: "6", profile: "15", userNaem: "Savbe"))
        objStory.append(Story(story: "5", profile: "16", userNaem: "Kolinn"))
        objStory.append(Story(story: "1", profile: "17", userNaem: "Khobshi"))
        
        //post object
        obj.append(Post(userName: "Jonny Prem", userProfile: "16", caption: " Use your own: Posting others’ images is not only copyright infringement, but it also makes your brand look cheap.", postImage: "11"))
        obj.append(Post(userName: "Tanh Him", userProfile: "12", caption: " You MUST read and comply with the allowed uses. I recommend double checking with the artist, as I did with Dani of Positively Present.", postImage: "9"))
        obj.append(Post(userName: "Leak Na", userProfile: "14", caption: " Some of the biggest social media influencers, like Guy Kawasaki, Kim Garst, and Sandi Krakowski, love to share inspirational quote graphics on their accounts.", postImage: "4"))
        obj.append(Post(userName: "You Long", userProfile: "15", caption: " Count me as a believer! I always get super engagement on posts with inspiring messages. And audience engagement is critical to organic (unpaid) reach on Facebook, Instagram.", postImage: "7"))
        obj.append(Post(userName: "Sok Thearak", userProfile: "18", caption:" Due to my nature, I’d love to encourage you to rethink this! 😉 Inspiring others can be a blessing for you as well as them. But it must come from a place of sincerity", postImage: "12"))
        
        //no Image Object
        objNoImg.append(NoImage(userName: "Lay La", userProfile: "1", caption: "Due to my nature, I’d love to encourage you to rethink this! 😉 Inspiring others can be a blessing for you as well as them. But it must come from a place of sincerity"))
        
        tableView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "tableViewCell")
//        tableView.register(UINib(nibName: "NoImageTableViewCell", bundle: nil), forCellReuseIdentifier: "noImageCell")
        tableView.delegate = self
        tableView.dataSource = self

        
        collectionView.register(UINib(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "storyCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        //remove select
        tableView.allowsSelection = false;
        
        //remove scroll
        self.collectionView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        
        
    }
}

extension ViewController: UICollectionViewDelegate{
    
}


extension ViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        objStory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storyCell", for: indexPath) as! CollectionViewCell
        
        cell.UserName.text = objStory[indexPath.row].userNaem
        cell.UserProfile.image = UIImage(named: objStory[indexPath.row].profile)
        cell.UserStroy.image = UIImage(named: objStory[indexPath.row].story)
        
        return cell
        
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 95, height: 170)
        }

}


extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  520
    }
}
extension ViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        obj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! PostTableViewCell
        
//        let noImageCell = tableView.dequeueReusableCell(withIdentifier: "noImageCell", for: indexPath) as! NoImageTableViewCell
        
        cell.userPrifile.image = UIImage(named: obj[indexPath.row].userProfile)
        cell.postImage.image = UIImage(named: obj[indexPath.row].postImage)
        cell.userName.text = obj[indexPath.row].userName
        cell.caption.text = obj[indexPath.row].caption
        
//        noImageCell.userName.text = objNoImg[indexPath.row].userName
//        noImageCell.caption.text = objNoImg[indexPath.row].caption
//        noImageCell.profile.image = UIImage(named: objNoImg[indexPath.row].userProfile)
        
       
            return cell
        

    }
    
}
