//
//  NoImageTableViewCell.swift
//  Facebook
//
//  Created by JONNY on 9/12/1399 AP.
//

import UIKit

class NoImageTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var caption: UITextView!
    @IBOutlet weak var userName: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
