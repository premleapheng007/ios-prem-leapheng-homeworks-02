//
//  PostItem.swift
//  Facebook
//
//  Created by JONNY on 9/10/1399 AP.
//

import Foundation

struct Post {
    var userName: String
    var userProfile: String
    var caption: String
    var postImage: String
}
