//
//  NoImage.swift
//  Facebook
//
//  Created by JONNY on 9/12/1399 AP.
//

import Foundation

struct NoImage {
    var userName: String
    var userProfile: String
    var caption: String
}
